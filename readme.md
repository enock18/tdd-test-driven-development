# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)
- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc,
  avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction

# Definition TDD

Le TDD est une technique de développement mêlant intimement l’écriture des tests unitaires, la programmation et l’amélioration continue du code (encore appelée refactorisation). Pour être unitaire, un test ne doit pas communiquer avec une base de données ni avec d’autres ressources ou systèmes d’informations sur le réseau, il ne manipule aucun fichier, il peut s’exécuter en même temps que les autres tests unitaires et il ne doit pas être lié à une autre fonctionnalité ou à un fichier de configuration pour être exécuté

# Les grands principes du TDD

1. Écrire un test

2. Vérifier qu’il échoue (puisqu’il n’y a pas de code correspondant)

3. Écrire le code suffisant pour que le test passe,

4. Vérifier que le test passe

5. Optimiser le code et vérifier qu’il n’y ait pas de régression.
