const expect = require("chai").expect;
const { calc } = require("../src/calc");

describe("calc", () => {
  it("Should return  num1 + num2", () => {
    expect(calc(1, 5, "+")).to.equal(6);
  });
  it("Should return  num1 - num2", () => {
    expect(calc(1, 4, "-")).to.equal(-3);
  });
  it("Should return  num1 / num2", () => {
    expect(calc(10, 5, "/")).to.equal(2);
  });
  it("Should return  num1 * num2", () => {
    expect(calc(3, 2, "*")).to.equal(6);
  });
  it("only 2 parameter ", () => {
    expect(calc(3, 2)).to.contain("Error!");
  });
  it("without parameter ", () => {
    expect(calc()).to.contain("Error!");
  });
});
