const fizzbuzz = (num) => {
  return !num
    ? "Error!"
    : num % 5 == 0 && num % 7 == 0
    ? "fizzbuzz"
    : num % 5 == 0
    ? "buzz"
    : num % 7 == 0
    ? "fizz"
    : "";
};

exports.fizzbuzz = fizzbuzz;
